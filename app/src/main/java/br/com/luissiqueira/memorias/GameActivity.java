package br.com.luissiqueira.memorias;

import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import br.com.luissiqueira.memoria.R;

import static android.provider.MediaStore.Images.*;

public class GameActivity extends ActionBarActivity {

    public static final String ARG_MODE_GAME = "mode_game";
    public static final int MODE_2_2 = 2;
    public static final int MODE_4_4 = 4;
    public static final int MODE_6_6 = 6;
    private static Object lock = new Object();
    private Cursor mCursor;
    private HashMap<String, Integer> mCardsPositions;
    private String[] mProjection = {Media._ID};
    private ArrayList<Integer> mPositions = new ArrayList<Integer>();
    private ArrayList<String> mDiscoveredCards = new ArrayList<String>();
    private static ImageView firstCard = null;
    private static ImageView secondCard = null;
    private static Integer firstCardId = null;
    private static Integer secondCardId = null;
    private UpdateCardsHandler handler;
    private int totalNeedImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_activity);
        getSupportActionBar().hide();

        firstCard = null;
        firstCardId = null;
        secondCard = null;
        secondCardId = null;

        int mode = getIntent().getIntExtra(ARG_MODE_GAME, 0);

        handler = new UpdateCardsHandler();

        if (mode != MODE_2_2 && mode != MODE_4_4 && mode != MODE_6_6) {
            Toast.makeText(getBaseContext(), R.string.invalid_mode_game, Toast.LENGTH_SHORT).show();
            finish();
        }

        mCursor = getContentResolver().query(Media.EXTERNAL_CONTENT_URI,
                mProjection, Media.DATA + " NOT LIKE ?", new String[]{"%WhatsApp%"}, null);

        int totalImages = mCursor.getCount();
        totalNeedImages = (mode * mode) / 2;

        if (totalImages < totalNeedImages) {
            Toast.makeText(getBaseContext(), R.string.images_not_found, Toast.LENGTH_LONG).show();
            finish();
        }

        while (mPositions.size() < totalNeedImages) {
            int position = new Random().nextInt(totalImages);
            if (!mPositions.contains(position)) {
                mPositions.add(position);
            }
        }

        if (savedInstanceState != null) {
            mCardsPositions = (HashMap<String, Integer>) savedInstanceState.getSerializable("mCardPositions");
            mDiscoveredCards = savedInstanceState.getStringArrayList("mDiscoveredCards");
        }

        createGame(mode);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putSerializable("mCardPositions", mCardsPositions);
        savedInstanceState.putStringArrayList("mDiscoveredCards", mDiscoveredCards);
    }

    private void createGame(int mode) {
        if (mCardsPositions == null) {
            List<String> positions = new ArrayList<String>();
            for (int y = 0; y < mode; y++) {
                for (int x = 0; x < mode; x++) {
                    positions.add(x + "," + y);
                }
            }
            mCardsPositions = new HashMap<String, Integer>();
            for (Integer id : mPositions) {
                int position = new Random().nextInt(positions.size());
                String keyPosition = positions.get(position);
                mCardsPositions.put(keyPosition, id);
                positions.remove(position);

                position = new Random().nextInt(positions.size());
                keyPosition = positions.get(position);
                mCardsPositions.put(keyPosition, id);
                positions.remove(position);
            }
        }

        LinearLayout root = (LinearLayout) findViewById(R.id.root);

        int size = getSizeImage(mode);

        mCursor.moveToFirst();
        for (int y = 0; y < mode; y++) {
            LinearLayout linearLayout = new LinearLayout(this);
            linearLayout.setGravity(Gravity.CENTER);
            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            for (int x = 0; x < mode; x++) {
                ImageView imageView = new ImageView(getBaseContext());
                imageView.setOnClickListener(new CardOnClickListener(x, y));
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(size, size);
                layoutParams.setMargins(4, 4, 4, 4);
                imageView.setLayoutParams(layoutParams);
                if (mDiscoveredCards.contains(x + "," + y)) {
                    imageView.setVisibility(View.INVISIBLE);
                } else {
                    imageView.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher));
                }
                imageView.setBackgroundColor(Color.argb(100, 255, 255, 255));
                linearLayout.addView(imageView);
                mCursor.moveToNext();
            }
            root.addView(linearLayout);
        }
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public int getSizeImage(int mode) {
        Display display = getWindowManager().getDefaultDisplay();
        int width, height, maxSize;

        int actionBarHeight = getSupportActionBar().getHeight();
        int statusBarHeight = getStatusBarHeight();

        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.HONEYCOMB_MR2) {
            width = display.getWidth();
            height = display.getHeight() - (actionBarHeight + statusBarHeight);
        } else {
            Point size1 = new Point();
            display.getSize(size1);
            width = size1.x;
            height = size1.y - (actionBarHeight + statusBarHeight);
        }

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            maxSize = height;
        } else {
            maxSize = width;
        }

        return (maxSize - (10 * (mode + 2))) / mode;
    }

    private class CardOnClickListener implements View.OnClickListener {

        int mX, mY;

        public CardOnClickListener(int x, int y) {
            mX = x;
            mY = y;
        }

        @Override
        public void onClick(View view) {
            if (firstCard != null && secondCard != null) {
                return;
            }

            int position = mCardsPositions.get(mX + "," + mY);
            mCursor.moveToPosition(position);
            long id = mCursor.getInt(0);
            ((ImageView) view).setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher));
            Bitmap bitmap = MediaStore.Images.Thumbnails.getThumbnail(
                    getContentResolver(), id, Thumbnails.MICRO_KIND, null);
            ((ImageView) view).setImageBitmap(bitmap);

            if (firstCard == null) {
                firstCard = (ImageView) view;
                firstCardId = mCursor.getInt(0);
            } else {
                if (firstCard.equals(view)) {
                    return;
                }

                secondCard = (ImageView) view;
                secondCardId = mCursor.getInt(0);

                TimerTask tt = new TimerTask() {

                    @Override
                    public void run() {
                        try {
                            synchronized (lock) {
                                handler.sendEmptyMessage(0);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                };

                Timer t = new Timer(false);
                t.schedule(tt, 900);
            }
        }
    }

    class UpdateCardsHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            synchronized (lock) {
                if (firstCardId.equals(secondCardId)) {
                    firstCard.setVisibility(View.INVISIBLE);
                    secondCard.setVisibility(View.INVISIBLE);
                    for (String k: mCardsPositions.keySet()) {
                        int position = mCardsPositions.get(k);
                        mCursor.moveToPosition(position);
                        if (mCursor.getInt(0) == firstCardId) {
                            mDiscoveredCards.add(k);
                        }
                    }
                    if (mDiscoveredCards.size() == totalNeedImages * 2) {
                        Toast.makeText(getBaseContext(), R.string.congratulations,
                                Toast.LENGTH_LONG).show();
                        finish();
                    }
                } else {
                    firstCard.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher));
                    secondCard.setImageDrawable(getResources().getDrawable(R.drawable.ic_launcher));
                }
                firstCard = null;
                secondCard = null;
            }
        }

    }


}
