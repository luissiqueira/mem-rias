package br.com.luissiqueira.memorias.util;

import android.content.Context;
import android.graphics.Typeface;

public class FontUtils {

    private static String gooddogPath = "fonts/gooddog.ttf";
    private static Typeface gooddog;

    public static Typeface getFontGooddog(Context ctx) {
        if (gooddog == null) {
            gooddog = Typeface.createFromAsset(ctx.getAssets(), gooddogPath);
        }
        return gooddog;
    }

}
