package br.com.luissiqueira.memorias;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import br.com.luissiqueira.memoria.R;
import br.com.luissiqueira.memorias.util.FontUtils;

import static android.provider.MediaStore.Images.*;

public class MainActivity extends ActionBarActivity {

    int[] idsTextViews = new int[] {R.id.tv_title_menu, R.id.tv_subtitle_menu,
            R.id.mode_4_4, R.id.mode_2_2, R.id.mode_6_6, R.id.about, R.id.exit,
            R.id.credits};

    int[] idsInitGames = new int[] {R.id.mode_6_6, R.id.mode_4_4, R.id.mode_2_2};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        String[] projection = { Media._ID };
            int totalImages = getContentResolver().query(Media.EXTERNAL_CONTENT_URI,
                    projection, Media.DATA + " NOT LIKE ?", new String[]{"%WhatsApp%"}, null)
                    .getCount();

        try {
            for (int idsTextView : idsTextViews) {
                ((TextView) findViewById(idsTextView)).setTypeface(FontUtils.getFontGooddog(this));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (totalImages < 18) {
            findViewById(R.id.mode_6_6).setVisibility(View.GONE);
        }

        if (totalImages < 8) {
            findViewById(R.id.mode_4_4).setVisibility(View.GONE);
        }

        if (totalImages < 4) {
            findViewById(R.id.mode_2_2).setVisibility(View.GONE);
            findViewById(R.id.divider).setVisibility(View.GONE);
            Toast.makeText(getBaseContext(), R.string.images_not_found, Toast.LENGTH_LONG).show();
        }

        for (final int id : idsInitGames) {
            findViewById(id).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startGame(id);
                }
            });
        }

        findViewById(R.id.about).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getBaseContext(), R.string.about_content, Toast.LENGTH_LONG).show();
            }
        });

        findViewById(R.id.exit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void startGame(int id) {
        Intent intent = new Intent(MainActivity.this, GameActivity.class);

        switch (id) {
            case R.id.mode_2_2:
                intent.putExtra(GameActivity.ARG_MODE_GAME, GameActivity.MODE_2_2);
                break;
            case R.id.mode_4_4:
                intent.putExtra(GameActivity.ARG_MODE_GAME, GameActivity.MODE_4_4);
                break;
            case R.id.mode_6_6:
                intent.putExtra(GameActivity.ARG_MODE_GAME, GameActivity.MODE_6_6);
                break;
            default:
                break;
        }

        if (intent.hasExtra(GameActivity.ARG_MODE_GAME)) {
            startActivity(intent);
        }
    }

}
